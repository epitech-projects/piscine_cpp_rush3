//
// IMonitorDisplay.hpp for rush3 in /home/delafo_b/tek2/rush/piscine_cpp_rush3
//
// Made by Brieuc de La Fouchardiere
// Login   <delafo_b@epitech.net>
//
// Started on  Sat Jan 25 18:06:22 2014 Brieuc de La Fouchardiere
// Last update Sun Jan 26 03:45:40 2014 Jean Gravier
//

#ifndef __IMONITORDISPLAY_HPP__
# define __IMONITORDISPLAY_HPP__

# include "../IMonitorModule/IMonitorModule.hpp"

class		IMonitorDisplay {

protected:
  float		_upDelay;

public:
  IMonitorDisplay();
  virtual ~IMonitorDisplay();

  virtual void	display() = 0;

};

#endif /* !__IMONITORDISPLAY_HPP__  */
