//
// mainwindow.cpp for Rush3 in /home/gravie_j/Documents/projets/fuque/qt/mygkrellm
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Sat Jan 25 01:40:12 2014 Jean Gravier
// Last update Sun Jan 26 08:45:34 2014 Jean Gravier
//

#include "QtMonitor.hpp"
#include "../../IMonitorModule/IMonitorModule.hpp"
#include "../../IMonitorModule/CPU.hpp"
#include <QPixmap>
#include <QString>
#include <QTimer>
#include <ctime>
#include <iostream>
#include <unistd.h>

QtMonitor::QtMonitor(QWidget *parent, float upDelay): QMainWindow(parent), IMonitorDisplay()
{
  /* Setting update delay frequency */
  this->_upDelay = upDelay;

 /* Module */
  this->_module = new IMonitorModule;

  /* Window Title */
  this->setWindowTitle("MystemSonitor+");

  /* Layouts Init */
  this->_mainLayout = new QVBoxLayout;
  this->_infoLayout = new QVBoxLayout;
  this->_cpuLayout = new QVBoxLayout;

  this->_topLayout = new QHBoxLayout;
  this->_groupLayout = new QHBoxLayout;
  this->_cpuBarLayout = new QHBoxLayout;
  this->_memLayout = new QHBoxLayout;
  this->_swapLayout = new QHBoxLayout;
  this->_netLayout = new QHBoxLayout;
  this->_netUpLayout = new QHBoxLayout;
  this->_netDownLayout = new QHBoxLayout;

  /* Widgets Init */
  this->_mainWidget = new QWidget(this);

  this->_userLabel = new QLabel("test", this);
  this->_dateLabel = new QLabel(this);
  this->_topLabel = new QLabel(this);
  this->_batteryLabel = new QLabel(this);
  this->_uptimeLabel = new QLabel(this);
  this->_cpuLabel = new QLabel(this);
  this->_cpuCoresLabel = new QLabel(this);
  this->_cpuFreqLabel = new QLabel(this);
  this->_cpuTempLabel = new QLabel(this);
  this->_cpuCacheLabel = new QLabel(this);
  this->_netUpLabel = new QLabel(this);
  this->_netDownLabel = new QLabel(this);

  this->_infoGroupBox = new QGroupBox("Info", this);
  this->_cpuGroupBox = new QGroupBox("Processor", this);
  this->_memGroupBox = new QGroupBox("Ram", this);
  this->_swapGroupBox = new QGroupBox("Swap", this);
  this->_netGroupBox = new QGroupBox("Network", this);

  this->_cpuBar = new QProgressBar(this);
  this->_memBar = new QProgressBar(this);
  this->_swapBar = new QProgressBar(this);
  this->_netUpArrow = new QLabel(this);
  this->_netDownArrow = new QLabel(this);

  /* Widgets Settings */
  this->_userLabel->setAlignment(Qt::AlignLeft);
  this->_dateLabel->setAlignment(Qt::AlignRight);
  this->_cpuLabel->setAlignment(Qt::AlignCenter);
  this->_netUpLayout->setAlignment(Qt::AlignCenter);
  this->_netDownLayout->setAlignment(Qt::AlignCenter);

  this->_memBar->setRange(0, this->_module->getTotalram());
  this->_swapBar->setRange(0, this->_module->getTotalswap());
  this->_memBar->setFormat("%v Mo");
  this->_swapBar->setFormat("%v Mo");

  this->_netUpArrow->setPixmap(QPixmap("IMonitorDisplay/Qt/img/arrow-up.png"));
  this->_netDownArrow->setPixmap(QPixmap("IMonitorDisplay/Qt/img/arrow-down.png"));

  /* Layout Affectations */
  this->_topLayout->addWidget(this->_userLabel);
  this->_topLayout->addWidget(this->_dateLabel);
  this->_infoLayout->addWidget(this->_topLabel);
  this->_infoLayout->addWidget(this->_batteryLabel);
  this->_infoLayout->addWidget(this->_uptimeLabel);
  this->_cpuBarLayout->addWidget(this->_cpuBar);
  this->_cpuLayout->addWidget(this->_cpuLabel);
  this->_netUpLayout->addWidget(this->_netUpArrow);
  this->_netUpLayout->addWidget(this->_netUpLabel);
  this->_netDownLayout->addWidget(this->_netDownArrow);
  this->_netDownLayout->addWidget(this->_netDownLabel);

  this->_cpuLayout->addLayout(this->_cpuBarLayout);

  this->_cpuLayout->addWidget(this->_cpuCoresLabel);
  this->_cpuLayout->addWidget(this->_cpuCacheLabel);
  this->_cpuLayout->addWidget(this->_cpuFreqLabel);
  this->_cpuLayout->addWidget(this->_cpuTempLabel);
  this->_memLayout->addWidget(this->_memBar);
  this->_swapLayout->addWidget(this->_swapBar);

  this->_netLayout->addLayout(this->_netUpLayout);
  this->_netLayout->addLayout(this->_netDownLayout);
  this->_mainLayout->addLayout(this->_topLayout);

  this->_mainLayout->addWidget(this->_infoGroupBox);
  this->_mainLayout->addWidget(this->_cpuGroupBox);
  this->_mainLayout->addWidget(this->_memGroupBox);
  this->_mainLayout->addWidget(this->_swapGroupBox);
  this->_mainLayout->addWidget(this->_netGroupBox);

  this->_infoGroupBox->setLayout(this->_infoLayout);
  this->_cpuGroupBox->setLayout(this->_cpuLayout);
  this->_memGroupBox->setLayout(this->_memLayout);
  this->_swapGroupBox->setLayout(this->_swapLayout);
  this->_netGroupBox->setLayout(this->_netLayout);

  this->_mainWidget->setLayout(this->_mainLayout);

  /* Main QWidget to QMainWindow */
  this->setCentralWidget(this->_mainWidget);
}

QtMonitor::~QtMonitor()
{
  /* Widgets delete */
  delete this->_userLabel;
  delete this->_dateLabel;
  delete this->_topLabel;
  delete this->_batteryLabel;
  delete this->_uptimeLabel;
  delete this->_cpuLabel;
  delete this->_cpuBar;
  delete this->_cpuCoresLabel;
  delete this->_cpuFreqLabel;
  delete this->_cpuTempLabel;
  delete this->_cpuCacheLabel;
  delete this->_netUpLabel;
  delete this->_netDownLabel;
  delete this->_memBar;
  delete this->_swapBar;

  /* Delete secondary Layouts */
  delete this->_groupLayout;
  delete this->_topLayout;
  delete this->_infoLayout;
  delete this->_cpuBarLayout;
  delete this->_cpuLayout;
  delete this->_memLayout;
  delete this->_swapLayout;
  delete this->_netUpLayout;
  delete this->_netDownLayout;
  delete this->_netLayout;

  /* Delete GroupBoxes */
  delete this->_infoGroupBox;
  delete this->_cpuGroupBox;
  delete this->_memGroupBox;
  delete this->_swapGroupBox;
  delete this->_netGroupBox;

  /* Delete main widget and layout at the end*/
  delete this->_mainLayout;
  delete this->_mainWidget;
}

void			QtMonitor::display()
{
  std::string		temp;
  QString		numbers;
  QString		hours;
  QString		minutes;
  QString		seconds;

  this->_module->retrieveAll();

  temp = "";
  temp += this->_module->getUsername();
  temp += "@";
  temp += this->_module->getHostname();

  this->_userLabel->setText(QString::fromStdString(temp));
  this->_dateLabel->setText(QString::fromStdString(this->_module->currentDateTime()));

  temp = this->_module->getOS();
  temp += " ";
  temp += this->_module->getKernel();

  this->_topLabel->setText(QString::fromStdString(temp));
  this->_cpuLabel->setText(QString::fromStdString(this->_module->_cpu->getModelName()));


  numbers = "Battery: " + numbers.setNum(this->_module->getBattery()) + "% ("
    + QString::fromStdString(this->_module->getBatteryStatus()) + ")";

  this->_batteryLabel->setText(numbers);

  hours.setNum(this->_module->getUptime() / 60 / 60);
  minutes.setNum((this->_module->getUptime() / 60) % 60);
  seconds.setNum(this->_module->getUptime() % 60);

  numbers = "Uptime: " + hours + "h " + minutes + "m " + seconds + "s";

  this->_uptimeLabel->setText(numbers);

  numbers = "Physical core(s): " + numbers.setNum(this->_module->_cpu->getNbCore());

  this->_cpuCoresLabel->setText(numbers);

  temp = "Cache: ";
  temp += this->_module->_cpu->getCacheSize();

  this->_cpuCacheLabel->setText(QString::fromStdString(temp));

  numbers = "Temperature: " + numbers.setNum(this->_module->_cpu->getTemperature()) + " °C";

  this->_cpuTempLabel->setText(numbers);

  numbers = "Frequency: " + numbers.setNum(this->_module->_cpu->getFrequency()) + " MHz";
  this->_cpuFreqLabel->setText(numbers);
  this->_cpuBar->setValue(this->_module->_cpu->getCPULoad());
  this->_memBar->setValue(this->_module->getTotalram() - this->_module->getFreeram());
  this->_swapBar->setValue(this->_module->getTotalswap() - this->_module->getFreeswap());

  numbers = numbers.setNum(this->_module->getUprate()) + " KB/s";
  this->_netUpLabel->setText(numbers);

  numbers = numbers.setNum(this->_module->getDownrate()) + " KB/s";
  this->_netDownLabel->setText(numbers);

  if (!this->isVisible())
    this->show();
  QTimer::singleShot(this->_upDelay * 1000, this, SLOT(display()));
}
