//
// QtMonitor.hpp for Rush3 in /home/gravie_j/Documents/projets/fuque/qt/mygkrellm
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Sat Jan 25 01:35:44 2014 Jean Gravier
// Last update Sun Jan 26 08:36:51 2014 Jean Gravier
//

#ifndef QTMONITOR_H
#define QTMONITOR_H

#include <QMainWindow>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QProgressBar>
#include <QLabel>
#include "../IMonitorDisplay.hpp"
#include "../../IMonitorModule/IMonitorModule.hpp"
#include "../../IMonitorModule/CPU.hpp"


class			QtMonitor : public QMainWindow, public IMonitorDisplay
{

  Q_OBJECT;

  public:
  explicit QtMonitor(QWidget *parent = 0, float = 0.1);
  ~QtMonitor();

public slots:
  void			display();

private:

  /* Main module */
  IMonitorModule	*_module;

  /* Layouts */
  QVBoxLayout		*_mainLayout;
  QVBoxLayout		*_cpuLayout;
  QVBoxLayout		*_infoLayout;

  QHBoxLayout		*_topLayout;
  QHBoxLayout		*_groupLayout;
  QHBoxLayout		*_cpuBarLayout;
  QHBoxLayout		*_memnetLayout;
  QHBoxLayout		*_memLayout;
  QHBoxLayout		*_swapLayout;
  QHBoxLayout		*_netLayout;
  QHBoxLayout		*_netUpLayout;
  QHBoxLayout		*_netDownLayout;

  /* Widgets */
  QWidget		*_mainWidget;

  QLabel		*_userLabel;
  QLabel		*_dateLabel;
  QLabel		*_topLabel;
  QLabel		*_batteryLabel;
  QLabel		*_uptimeLabel;
  QLabel		*_cpuLabel;
  QLabel		*_cpuCoresLabel;
  QLabel		*_cpuCacheLabel;
  QLabel		*_cpuFreqLabel;
  QLabel		*_cpuTempLabel;
  QLabel		*_netUpArrow;
  QLabel		*_netDownArrow;
  QLabel		*_netUpLabel;
  QLabel		*_netDownLabel;

  QGroupBox		*_infoGroupBox;
  QGroupBox		*_cpuGroupBox;
  QGroupBox		*_memGroupBox;
  QGroupBox		*_swapGroupBox;
  QGroupBox		*_netGroupBox;

  QProgressBar		*_cpuBar;
  QProgressBar		*_memBar;
  QProgressBar		*_swapBar;
};

#endif // QTMONITOR_H
