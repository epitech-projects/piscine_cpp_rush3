#include <ncurses.h>
#include <unistd.h>
#include "NCursesMonitor.hpp"
#include "../IMonitorDisplay.hpp"
#include "../../IMonitorModule/CPU.hpp"
#include "../../IMonitorModule/IMonitorModule.hpp"

void	NCursesMonitor::display()
{
  IMonitorModule	module;
  module.retrieveAll();
  while(getch() != 'q')
    {
      if (COLS < 80 || LINES < 44)
	{
	  init_pair(2, COLOR_RED, COLOR_BLACK);
	  attron(COLOR_PAIR(2));
	  mvprintw((LINES + 2) / 2, (COLS - 24) / 2, "Please expand the window");
	  attroff(COLOR_PAIR(2));
	}
      _generateBox(module);
      _generateInfo(module);
      refresh();
      module.retrieveAll();
      usleep(this->_upDelay);
      _destroyBox();
      _destroyInfo();
    }
  endwin();
}

/*****************/
/* INFO FUNCTION */
/*****************/
void	NCursesMonitor::_drawOSInfo(IMonitorModule& module)
{
  wattron(this->_info, A_REVERSE);
  mvwprintw(this->_info, 1, 1, " OS Info ");
  wattroff(this->_info, A_REVERSE);
  mvwhline(this->_info, 1, 10, ACS_HLINE, COLS);
  mvwprintw(this->_info, 3, 2, "> Operating System :\t%s", (module.getOS()).c_str());
  mvwprintw(this->_info, 4, 2, "> Kernel Version :\t%s", (module.getKernel()).c_str());
}

void	NCursesMonitor::_drawCPU(IMonitorModule& module)
{
  wattron(this->_info, A_REVERSE);
  mvwprintw(this->_info, 6, 1, " CPU Info ");
  wattroff(this->_info, A_REVERSE);
  mvwhline(this->_info, 6, 11, ACS_HLINE, COLS);
  mvwprintw(this->_info, 8, 2, "> CPU Model :\t\t%s", (module._cpu->getModelName()).c_str());
  mvwprintw(this->_info, 9, 2, "> Frequency :\t\t%.2f Hz\t\t > Physical core(s) : %u", module._cpu->getFrequency(), module._cpu->getNbCore());
  mvwprintw(this->_info, 11, 2, "> Cache Size :\t%s", (module._cpu->getCacheSize()).c_str());
  mvwprintw(this->_info, 10, 2, "> CPU Temperature :\t%d°C", module._cpu->getTemperature());
  _drawCPUGraph(module._cpu->getCPULoad());
}

void	NCursesMonitor::_drawCPUGraph(unsigned int percent)
{
  if (percent > 100)
    percent = 100;
  mvwprintw(this->_info, 13, 24, "|0\%");
  mvwprintw(this->_info, 13, 49, "|50\%");
  mvwprintw(this->_info, 13, 74, "|100\%");
  mvwprintw(this->_info, 14, 2, "> CPU Utilisation : ");
  for (unsigned int i = 0; i < (percent / 2); ++i)
    {
      if (i > 37)
	{
	  wattron(this->_info, COLOR_PAIR(2));
	  mvwprintw(this->_info, 14, (24 + i), "|");
	  wattroff(this->_info, COLOR_PAIR(2));
	}
      else
	mvwprintw(this->_info, 14, (24 + i), "|");
    }
  wattron(this->_info, COLOR_PAIR(1));
}

void	NCursesMonitor::_drawRAM(IMonitorModule& module)
{
  float	tmp;
  wattron(this->_info, A_REVERSE);
  mvwprintw(this->_info, 16, 1, " RAM Info ");
  wattroff(this->_info, A_REVERSE);
  mvwhline(this->_info, 16, 11, ACS_HLINE, COLS);
  mvwprintw(this->_info, 18, 2, "> RAM Capacity :\t%u MB", module.getTotalram());
  tmp = module.getTotalram() - module.getFreeram();
  _drawRAMGraph((tmp / module.getTotalram()) * 100.0);
}

void	NCursesMonitor::_drawRAMGraph(unsigned int percent)
{
  if (percent > 100)
    percent = 100;

  mvwprintw(this->_info, 20, 24, "|0\%");
  mvwprintw(this->_info, 20, 49, "|50\%");
  mvwprintw(this->_info, 20, 74, "|100\%");
  mvwprintw(this->_info, 21, 2, "> RAM Utilisation : ");
  for (unsigned int i = 0; i < (percent / 2.00); ++i)
    {
      if (i > 37)
	{
	  wattron(this->_info, COLOR_PAIR(2));
	  mvwprintw(this->_info, 21, (24 + i), "|");
	  wattroff(this->_info, COLOR_PAIR(2));
	}
      else
	mvwprintw(this->_info, 21, (24 + i), "|");
    }
  wattron(this->_info, COLOR_PAIR(1));
}

void	NCursesMonitor::_drawSwap(IMonitorModule& module)
{
  float	tmp;
  wattron(this->_info, A_REVERSE);
  mvwprintw(this->_info, 23, 1, " Swap Info ");
  wattroff(this->_info, A_REVERSE);
  mvwhline(this->_info, 23, 12, ACS_HLINE, COLS);
  mvwprintw(this->_info, 25, 2, "> Swap Capacity :\t%u MB", module.getTotalswap());
  tmp = module.getTotalswap() - module.getFreeswap();
  _drawSwapGraph((tmp / module.getTotalswap()) * 100.0);
}

void	NCursesMonitor::_drawSwapGraph(unsigned int percent)
{
  if (percent > 100)
    percent = 100;

  mvwprintw(this->_info, 27, 24, "|0\%");
  mvwprintw(this->_info, 27, 49, "|50\%");
  mvwprintw(this->_info, 27, 74, "|100\%");
  mvwprintw(this->_info, 28, 2, "> Swap Utilisation : ");
  for (unsigned int i = 0; i < (percent / 2.00); ++i)
    {
      if (i > 37)
	{
	  wattron(this->_info, COLOR_PAIR(2));
	  mvwprintw(this->_info, 28, (24 + i), "|");
	  wattroff(this->_info, COLOR_PAIR(2));
	}
      else if (i < 37)
	mvwprintw(this->_info, 28, (24 + i), "|");
    }
  wattron(this->_info, COLOR_PAIR(1));
}

void	NCursesMonitor::_drawNetwork(IMonitorModule& module)
{
  wattron(this->_info, A_REVERSE);
  mvwprintw(this->_info, 30, 1, " Network Info ");
  wattroff(this->_info, A_REVERSE);
  mvwhline(this->_info, 30, 15, ACS_HLINE, COLS);
  mvwprintw(this->_info, 32, 2, "> Download speed:\t%u KB/S", module.getDownrate());
  mvwprintw(this->_info, 33, 2, "> Upload speed:\t%u KB/S", module.getUprate());
}

void	NCursesMonitor::_drawBattery(IMonitorModule& module)
{
  wattron(this->_info, A_REVERSE);
  mvwprintw(this->_info, 35, 1, " Battery Info ");
  wattroff(this->_info, A_REVERSE);
  mvwhline(this->_info, 35, 15, ACS_HLINE, COLS);
  mvwprintw(this->_info, 37, 2, "> Battery Status :\t%s", (module.getBatteryStatus()).c_str());
  _drawBatteryGraph(module.getBattery());
}

void	NCursesMonitor::_drawBatteryGraph(unsigned int percent)
{
  if (percent > 100)
    percent = 100;
  mvwprintw(this->_info, 39, 24, "|0\%");
  mvwprintw(this->_info, 39, 49, "|50\%");
  mvwprintw(this->_info, 39, 74, "|100\%");
  mvwprintw(this->_info, 40, 2, "> Battery Charge : ");
  for (unsigned int i = 0; i < (percent / 2.00); ++i)
    mvwprintw(this->_info, 40, (24 + i), "|");
}

void	NCursesMonitor::_generateInfo(IMonitorModule& module)
{
  this->_info = newwin(LINES - 2, COLS - 2, 1, 1);
  wattron(this->_info, COLOR_PAIR(1));
  _drawOSInfo(module);
  _drawCPU(module);
  _drawRAM(module);
  _drawNetwork(module);
  _drawSwap(module);
  _drawBattery(module);
  wrefresh(this->_info);
  wattroff(this->_info, COLOR_PAIR(1));
}

void	NCursesMonitor::_destroyInfo()
{
  wbkgd(this->_info, COLOR_PAIR(1));
  wrefresh(this->_info);
  delwin(this->_info);
}

/*****************/
/* BOX FUNCTIONS */
/*****************/
void	NCursesMonitor::_drawUpBar(IMonitorModule& module)
{
  mvwprintw(this->_box, 0, 2, "MystemSonitor");
  mvwprintw(this->_box, 0, 20, "%s@%s", (module.getUsername()).c_str(),
	    (module.getHostname()).c_str());
  mvwprintw(this->_box, 0, COLS - 18, "%s", (module.currentDateTime()).c_str());
}

void	NCursesMonitor::_drawBottomBar(IMonitorModule& module)
{
  mvwprintw(this->_box, LINES - 1, 2, "(q) Quit");
  _convertUptime(module.getUptime());
}

void	NCursesMonitor::_convertUptime(long seconds)
{
  int	hours;
  int	mins_left;
  int	secs_left;

  hours = seconds / 3600;
  seconds = seconds % 3600;
  mins_left = seconds / 60;
  seconds = seconds % 60;
  secs_left = seconds;
  mvwprintw(this->_box, LINES - 1, COLS - 20, "Uptime: %d:%d:%d", hours, mins_left, secs_left);
}

void	NCursesMonitor::_generateBox(IMonitorModule& module)
{
  this->_box = newwin(LINES, COLS, 0, 0);
  wattron(this->_box, COLOR_PAIR(1));
  box(this->_box, 0 , 0);
  _drawUpBar(module);
  _drawBottomBar(module);
  wattroff(this->_box, COLOR_PAIR(1));
  wrefresh(this->_box);
}

void	NCursesMonitor::_destroyBox()
{
  wborder(this->_box, ' ', ' ', ' ',' ',' ',' ',' ',' ');
  wrefresh(this->_box);
  delwin(this->_box);
}

/**************************/
/* INIT-DESTROY FUNCTIONS */
/**************************/
NCursesMonitor::NCursesMonitor(float delay) : IMonitorDisplay()
{
  initscr();
  cbreak();
  noecho();
  curs_set(0);
  keypad(stdscr, TRUE);
  nodelay(stdscr, TRUE);
  start_color();
  if (has_colors() == TRUE)
    {
      init_color(COLOR_BLACK, 128, 128, 128);
      init_color(COLOR_CYAN, 119, 136, 153);
      init_pair(1, COLOR_CYAN, COLOR_BLACK);
      init_pair(2, COLOR_RED, COLOR_BLACK);
    }
  this->_upDelay = (delay * 1000000);
}

NCursesMonitor::~NCursesMonitor()
{
}

