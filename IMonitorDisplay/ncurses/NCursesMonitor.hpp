#ifndef _NCURSES_H_
# define _NCURSES_H_

# include <ncurses.h>
# include "../IMonitorDisplay.hpp"
# include "../../IMonitorModule/CPU.hpp"
# include "../../IMonitorModule/IMonitorModule.hpp"

class	NCursesMonitor : public IMonitorDisplay
{
private:
  WINDOW	*_box;
  WINDOW	*_info;

public:
  NCursesMonitor(float = 0.1);
  ~NCursesMonitor();
  void		display();

private:
  void		_generateBox(IMonitorModule&);
  void		_destroyBox();
  void		_convertUptime(long);
  void		_drawUpBar(IMonitorModule&);
  void		_drawBottomBar(IMonitorModule&);

  void		_generateInfo(IMonitorModule&);
  void		_destroyInfo();
  void		_drawOSInfo(IMonitorModule&);
  void		_drawCPU(IMonitorModule&);
  void		_drawCPUGraph(unsigned int);
  void		_drawRAM(IMonitorModule&);
  void		_drawRAMGraph(unsigned int);
  void		_drawSwap(IMonitorModule&);
  void		_drawSwapGraph(unsigned int);
  void		_drawNetwork(IMonitorModule&);
  void		_drawBattery(IMonitorModule&);
  void		_drawBatteryGraph(unsigned int);
};

#endif /* _NCURSES_H_ */
