//
// main.cpp for Rush3 in /home/gravie_j/Documents/projets/piscine/piscine_cpp_rush3
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Sat Jan 25 23:12:08 2014 Jean Gravier
// Last update Sun Jan 26 06:42:36 2014 Jean Gravier
//

#include <QApplication>
#include <iostream>
#include "IMonitorModule/CPU.hpp"
#include "IMonitorModule/IMonitorModule.hpp"
#include "IMonitorDisplay/IMonitorDisplay.hpp"
#include "IMonitorDisplay/Qt/QtMonitor.hpp"
#include "IMonitorDisplay/ncurses/NCursesMonitor.hpp"

int		main(int argc, char **argv)
{
  std::string	mode;

  if (argc > 1)
    {
      mode = argv[1];
      if (mode == "--gui")
	{
	  QApplication app(argc, argv);
	  QtMonitor monitor;
	  monitor.display();
	  return app.exec();
	}
    }
  NCursesMonitor cursesMonitor;

  cursesMonitor.display();
  return 0;
}
