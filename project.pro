#-------------------------------------------------
#
# Project created by QtCreator 2014-01-25T01:14:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mystem-sonitor
TEMPLATE = app

QMAKE_CXXFLAGS += -W -Wall -Wextra -Werror

LIBS += -lncurses

INCLUDEPATH += . \
               IMonitorModule \
               IMonitorDisplay \
               IMonitorDisplay/ncurses \
               IMonitorDisplay/Qt

# Input
HEADERS += IMonitorDisplay/IMonitorDisplay.hpp \
           IMonitorModule/CPU.hpp \
           IMonitorModule/IMonitorModule.hpp \
           IMonitorDisplay/ncurses/NCursesMonitor.hpp \
           IMonitorDisplay/Qt/QtMonitor.hpp

SOURCES += main.cpp \
           IMonitorDisplay/IMonitorDisplay.cpp \
           IMonitorModule/CPU.cpp \
           IMonitorModule/IMonitorModule.cpp \
           IMonitorDisplay/ncurses/NCursesMonitor.cpp \
           IMonitorDisplay/Qt/QtMonitor.cpp
