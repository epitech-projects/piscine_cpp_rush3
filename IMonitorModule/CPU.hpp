//
// CPU.hpp for  in /home/fritsc_h/piscine_cpp_rush3/IMonitorModule
// 
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
// 
// Started on  Sun Jan 26 02:26:18 2014 Fritsch harold
// Last update Sun Jan 26 08:36:58 2014 Fritsch harold
//

#ifndef CPU_HPP_
# define CPU_HPP_
# include <string>
# include <vector>

class				CPU
{
public:
  CPU();
  CPU(const CPU &);
  virtual ~CPU();
  void				operator=(const CPU &);
  std::string			getModelName() const;
  float				getFrequency() const;
  std::string			getCacheSize() const;
  int				getTemperature() const;
  size_t			getNbCore() const;
  size_t			getCPULoad() const;
  void				feedCPUInfo();
  void				retrieveCPULoad();
  void				retrieveCPUTemp();
  std::vector<int>		retrieveCoreLoad();
  std::string			cutThat(std::string, std::string);
    
private:
  std::string			_model_name;
  float				_frequency;
  std::string			_cache_size;
  int				_temperature;
  size_t			_cpu_load;
  size_t			_nb_core;
};

#endif /* !CPU_HPP_ */
