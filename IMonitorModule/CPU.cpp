//
// CPU.cpp for  in /home/fritsc_h/piscine_cpp_rush3/IMonitorModule
//
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
//
// Started on  Sun Jan 26 02:26:42 2014 Fritsch harold
// Last update Sun Jan 26 09:04:48 2014 Jean Gravier
//

#include <iomanip>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <cstdlib>
#include <map>
#include <string>
#include "CPU.hpp"

CPU::CPU()
{
  feedCPUInfo();
}

CPU::CPU(const CPU &to_cpy)
{
  _model_name = to_cpy.getModelName();
  _frequency = to_cpy.getFrequency();
  _cache_size = to_cpy.getCacheSize();
  _temperature = to_cpy.getTemperature();
  _cpu_load = to_cpy.getCPULoad();
}

CPU::~CPU()
{
  ;
}

void			CPU::operator=(const CPU &to_cpy)
{
  _model_name = to_cpy.getModelName();
  _frequency = to_cpy.getFrequency();
  _cache_size = to_cpy.getCacheSize();
  _temperature = to_cpy.getTemperature();
  _cpu_load = to_cpy.getCPULoad();
}

std::string		CPU::getModelName() const
{
  return (_model_name);
}

float			CPU::getFrequency() const
{
  return (_frequency);
}

std::string		CPU::getCacheSize() const
{
  return (_cache_size);
}

int			CPU::getTemperature() const
{
  return (_temperature);
}

size_t			CPU::getCPULoad() const
{
  return (_cpu_load);
}

std::string		CPU::cutThat(std::string to_find, std::string data)
{
  size_t		start_pos;
  size_t		end_pos;

  start_pos = data.find(to_find, 0) + to_find.length(); //assigne la position de la fin de model name:
  if (start_pos - to_find.length() == std::string::npos)
    return ("");
  end_pos = data.find("\n", start_pos); //assigne la position du \n qui est après model name
  return (data.substr(start_pos, end_pos - start_pos));
}

size_t			CPU::getNbCore() const
{
  return (_nb_core);
}

void			CPU::feedCPUInfo()
{
  std::ifstream		src("/proc/cpuinfo");
  std::stringstream	buffer;
  std::string		data;
  std::string		temp;
  size_t		loc = -1;

  if (src.is_open())
    {
      buffer << src.rdbuf();
      data = buffer.str();
      src.close();

      while((loc = data.find("\t", loc + 1)) != std::string::npos) //supprime les tabulations
	data.replace(loc,1,"");
      loc = -1;
      while((loc = data.find("  ", loc + 1)) != std::string::npos) //supprime les doublons d'espace
	data.replace(loc,2," ");
      loc = -1;
      while((loc = data.find("\t", loc + 1)) != std::string::npos) //supprime les tabulations restantes
	data.replace(loc,1,"");
      _model_name = this->cutThat("model name: ", data);
      temp = this->cutThat("cpu MHz: ", data);

      std::stringstream ss(temp);
      ss >> _frequency;

      _cache_size = this->cutThat("cache size: ", data);
      ss.clear();
      temp = this->cutThat("cpu cores: ", data);
      ss.str(temp);
      ss >> _nb_core;
    }
  else
    std::cout << "open failed [feed_cpuinfo]" << std::endl;
  this->retrieveCPULoad();
  this->retrieveCPUTemp();
}

void				CPU::retrieveCPUTemp()
{
  std::ifstream			src("/sys/class/thermal/thermal_zone0/temp");
  std::stringstream		buffer;
  std::string			data;

  if (src.is_open())
    {
      buffer.str("");
      buffer << src.rdbuf();
      data = buffer.str();
      src.close();

      std::stringstream ss(data);
      ss >> _temperature;
      _temperature = _temperature / 1000;
    }
}

void				CPU::retrieveCPULoad()
{
  std::vector<int>		vec1;
  std::vector<int>		vec2;
  int				tot_jiffies_1 = 0;
  int				tot_jiffies_2 = 0;
  int				work_jiffies_1 = 0;
  int				work_jiffies_2 = 0;
  int				work_over_period = 0;
  int				total_over_period = 0;
  size_t			it = 0;
  float				percentage;

  vec1 = retrieveCoreLoad();
  usleep(100000);
  vec2 = retrieveCoreLoad();
  while (it < 3)
    {
      work_jiffies_1 += vec1[it];
      work_jiffies_2 += vec2[it];
      ++it;
    }
  it = 0;
  while (it < 10)
    {
      tot_jiffies_1 += vec1[it];
      tot_jiffies_2 += vec2[it];
      ++it;
    }
  work_over_period = work_jiffies_2 - work_jiffies_1;
  total_over_period = tot_jiffies_2 - tot_jiffies_1;
  percentage = (float)work_over_period / (float)total_over_period * 100.00;
  _cpu_load = (int)percentage;
}

std::vector<int>	CPU::retrieveCoreLoad()
{
  std::ifstream			src("/proc/stat");
  std::stringstream		buffer;
  std::string			data;
  std::string			temp;
  std::vector<int>		ret;
  int				tmp;

  if (src.is_open())
    {
      buffer << src.rdbuf();
      data = buffer.str();
      src.close();
      temp = this->cutThat("cpu ", data);
      std::istringstream	split(temp);
      while (std::getline(split, temp, ' '))
	{
	  tmp = 0;
	  std::stringstream ss(temp);
	  ss >> tmp;
	  ret.push_back(tmp);
	}
    }
  return (ret);
}
