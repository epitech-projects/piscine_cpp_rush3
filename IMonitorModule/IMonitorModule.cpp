//
// IMonitorModule.cpp for  in /home/fritsc_h/piscine_cpp_rush3/IMonitorModule
//
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
//
// Started on  Sun Jan 26 06:59:15 2014 Fritsch harold
// Last update Sun Jan 26 09:05:41 2014 Jean Gravier
//

#include <sys/utsname.h>
#include <sys/sysinfo.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <time.h>
#include <unistd.h>
#include "IMonitorModule.hpp"
#include "CPU.hpp"

IMonitorModule::IMonitorModule()
{
  this->_cpu = new CPU;
  this->retrieveAll();
}

IMonitorModule::IMonitorModule(const IMonitorModule &to_cpy)
{
  _OS = to_cpy._OS;
  _kernel = to_cpy._kernel;
  _hostname = to_cpy._hostname;
  _username = to_cpy._username;
}

IMonitorModule::~IMonitorModule()
{
  delete this->_cpu;
}

void			IMonitorModule::operator=(const IMonitorModule &to_cpy)
{
  _OS = to_cpy._OS;
  _kernel = to_cpy._kernel;
  _hostname = to_cpy._hostname;
  _username = to_cpy._username;
}

void			IMonitorModule::retrieveAll()
{
  struct		utsname	buf;
  struct		sysinfo	sys;

  uname(&buf);
  sysinfo(&sys);
  retrieveOS();
  _kernel = buf.release;
  _hostname = buf.nodename;
  _username = getlogin();
  _uptime = sys.uptime;
  _totalram = sys.totalram;
  _freeram = sys.freeram;
  _totalswap = sys.totalswap;
  _freeswap = sys.freeswap;
  this->_cpu->feedCPUInfo();
  this->retrieveUprate();
  this->retrieveDownrate();
  this->retrieveBattery();
  this->retrieveBatteryStatus();
}

long			IMonitorModule::getUptime() const
{
  return (_uptime);
}

unsigned long		IMonitorModule::getTotalram() const
{
  return (_totalram / (1024 * 1024));
}

unsigned long		IMonitorModule::getFreeram() const
{
  return (_freeram / (1024 * 1024));
}

unsigned long		IMonitorModule::getTotalswap() const
{
  return (_totalswap / (1024 * 1024));
}

unsigned long		IMonitorModule::getFreeswap() const
{
  return (_freeswap / (1024 * 1024));
}

size_t			IMonitorModule::getBattery() const
{
  return (_battery);
}

std::string		IMonitorModule::retrieveOS()
{
  std::ifstream		src("/etc/os-release");
  std::stringstream	buffer;
  std::string		data;
  size_t		start_pos;
  size_t		end_pos;

  if (src.is_open())
    {
      buffer << src.rdbuf();
      data = buffer.str();
      src.close();
      start_pos = data.find("PRETTY_NAME=\"", 0) + 13; //assigne la position de la fin de PRETTY_NAME
      end_pos = data.find("\"", start_pos); //assigne la position du " d'après PRETTY_NAME
      _OS = data.substr(start_pos, end_pos - start_pos);//extrait la chaine et la remplit dans _OS
    }
  else
    _OS = "n/a";
  return (_OS);
}

std::string		IMonitorModule::getOS() const
{
  return (_OS);
}

std::string		IMonitorModule::getKernel() const
{
  return (_kernel);
}

std::string		IMonitorModule::getHostname() const
{
  return (_hostname);
}

std::string		IMonitorModule::getUsername() const
{
  return (_username);
}

std::string		IMonitorModule::currentDateTime()
{
  time_t     now = time(0);
  struct tm  tstruct;
  char       buf[20];
  tstruct = *localtime(&now);
  strftime(buf, sizeof(buf), "%d/%m/%Y %H:%M", &tstruct);
  return buf;
}

void			IMonitorModule::retrieveUprate()
{
  std::ifstream			src_wlan0("/sys/class/net/wlan0/statistics/tx_bytes");
  std::stringstream		buffer;
  std::string			data;
  int				tmp1 = 0;
  int				tmp2 = 0;

  if (src_wlan0.is_open())
    {
      buffer.str("");
      buffer << src_wlan0.rdbuf();
      data = buffer.str();
      src_wlan0.close();
      std::stringstream ss(data);
      ss >> tmp1;
      usleep(100000);

      std::ifstream	src_wlan0("/sys/class/net/wlan0/statistics/tx_bytes");
      if (src_wlan0.is_open())
	{
	  data = "";
	  buffer.str("");
	  buffer << src_wlan0.rdbuf();
	  data = buffer.str();
	  src_wlan0.close();
	  std::stringstream ss(data);
	  ss >> tmp2;
	}
      _uprate = (tmp2 - tmp1) * 10 / 1024;
    }
}

size_t			IMonitorModule::getUprate() const
{
  return (_uprate);
}

size_t			IMonitorModule::getDownrate() const
{
  return (_downrate);
}

void			IMonitorModule::retrieveDownrate()
{
  std::ifstream			src_wlan0("/sys/class/net/wlan0/statistics/rx_bytes");
  std::stringstream		buffer;
  std::string			data;
  int				tmp1;
  int				tmp2;

  if (src_wlan0.is_open())
    {
      buffer << src_wlan0.rdbuf();
      data = buffer.str();
      src_wlan0.close();
      std::stringstream ss(data);
      ss >> tmp1;

      usleep(100000);

      std::ifstream	src_wlan0("/sys/class/net/wlan0/statistics/rx_bytes");
      if (src_wlan0.is_open())
	{
	  data = "";
	  buffer.str("");
	  buffer << src_wlan0.rdbuf();
	  data = buffer.str();
	  src_wlan0.close();
	  std::stringstream ss(data);
	  ss >> tmp2;
	}
      _downrate = (tmp2 - tmp1) * 10 / 1024;
    }
}

void				IMonitorModule::retrieveBattery()
{
  std::ifstream			src_bat0("/sys/class/power_supply/BAT0/capacity");
  std::ifstream			src_bat1("/sys/class/power_supply/BAT1/capacity");
  std::stringstream		buffer;
  std::string			data;

  if (src_bat0.is_open())
    {
      buffer.str("");
      buffer << src_bat0.rdbuf();
      data = buffer.str();
      src_bat0.close();

      std::stringstream ss(data);
      ss >> _battery;
    }
  else if (src_bat1.is_open())
    {
      buffer.str("");
      buffer << src_bat1.rdbuf();
      data = buffer.str();
      src_bat1.close();

      std::stringstream ss(data);
      ss >> _battery;
    }
}

void				IMonitorModule::retrieveBatteryStatus()
{
  std::ifstream			src_bat0("/sys/class/power_supply/BAT0/status");
  std::ifstream			src_bat1("/sys/class/power_supply/BAT1/status");
  std::stringstream		buffer;
  std::string			data;

  if (src_bat0.is_open())
    {
      buffer.str("");
      buffer << src_bat0.rdbuf();
      data = buffer.str();
      src_bat0.close();
      _batteryStatus = data;
      _batteryStatus.erase(_batteryStatus.end() - 1);
    }
  else if (src_bat1.is_open())
    {
      buffer.str("");
      buffer << src_bat1.rdbuf();
      data = buffer.str();
      src_bat1.close();
      _batteryStatus = data;
      _batteryStatus.erase(_batteryStatus.end() - 1);
    }
  if (_batteryStatus == "Unknown")
    _batteryStatus = "Charging";
}

std::string			IMonitorModule::getBatteryStatus() const
{
  return (_batteryStatus);
}
