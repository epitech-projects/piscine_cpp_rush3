//
// IMonitorModule.hpp for  in /home/fritsc_h/piscine_cpp_rush3
//
// Made by Fritsch harold
// Login   <fritsc_h@epitech.net>
//
// Started on  Sat Jan 25 03:44:59 2014 Fritsch harold
// Last update Sun Jan 26 07:30:09 2014 Fritsch harold
//

#ifndef IMONITORMODULE_HPP_
# define IMONITORMODULE_HPP_
# include <string>
# include "CPU.hpp"

class			IMonitorModule
{
public:
  IMonitorModule();
  IMonitorModule(const IMonitorModule&);
  ~IMonitorModule();
  void			operator=(const IMonitorModule&);
  std::string		getOS() const;
  std::string		getKernel() const;
  std::string		getHostname() const;
  std::string		getUsername() const;
  std::string		retrieveOS();
  std::string		currentDateTime();
  long			getUptime() const;
  void			retrieveAll();
  CPU			*_cpu;

  /*memory*/
  unsigned long		getTotalram() const;
  unsigned long		getFreeram() const;
  unsigned long		getTotalswap() const;
  unsigned long		getFreeswap() const;

  /*net*/
  size_t		getUprate() const;
  size_t		getDownrate() const;
  void			retrieveUprate();
  void			retrieveDownrate();
  
  
  /*battery*/
  size_t		getBattery() const;
  std::string		getBatteryStatus() const;
  void			retrieveBattery();
  void			retrieveBatteryStatus();


private:
  struct sysinfo	*_sys;
  std::string		_OS;
  std::string		_kernel;
  std::string		_hostname;
  std::string		_username;
  long			_uptime;
  unsigned long		_totalram;
  unsigned long		_freeram;
  unsigned long		_totalswap;
  unsigned long		_freeswap;
  size_t		_uprate;
  size_t		_downrate;
  size_t		_battery;
  std::string		_batteryStatus;
};

#endif /* !IMONITORMODULE_HPP_ */
